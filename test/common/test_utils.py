import fiona
import pytest
import os
import geopandas
from shapely.geometry import Polygon

from common.errors import FileNotGoodFormat, ShapeNotSupported
from common.utils import JSONFile


def test_read_json_file(mocker):
    dir_name = 'test_dir'
    file_name = 'file.json'
    mocker.patch('geopandas.read_file', return_value='test')
    json_file = JSONFile(dir_name, file_name)
    geopandas.read_file.assert_called_once_with(
        os.path.join(dir_name, file_name)
    )
    assert json_file._file == 'test'
    assert json_file._dir_path == dir_name
    assert json_file._file_path == file_name
    file_name = json_file.generate_file_name()
    assert file_name == 'file'


def test_invalid_json_file(mocker):
    dir_name = 'test_dir'
    file_name = 'file.json'
    mocker.patch(
        'geopandas.read_file', side_effect=fiona.errors.DriverError()
    )
    with pytest.raises(FileNotGoodFormat):
        JSONFile(dir_name, file_name)
        geopandas.read_file.assert_called_once_with(
            os.path.join(dir_name, file_name)
        )


def test_generate_report_polygons(mocker):
    dir_name = 'test_dir'
    file_name = 'file.json'
    file_data = [
        (0, {'geometry': Polygon()})
    ]
    iter_file_data = iter(file_data)
    frame = geopandas.GeoDataFrame()

    mocker.patch.object(frame, 'iterrows', return_value=iter_file_data)
    mocker.patch(
        'geopandas.read_file', return_value=frame
    )
    json_file = JSONFile(dir_name, file_name)
    mocker.patch.object(
        json_file, '_polygons_report', return_value='test'
    )
    json_file._reporters = {
        Polygon: json_file._polygons_report
    }
    json_file.generate_report()
    json_file._polygons_report.assert_called_once()


def test_generate_invalid_shape(mocker):
    dir_name = 'test_dir'
    file_name = 'file.json'
    file_data = [
        (0, {'geometry': Exception()})
    ]
    iter_file_data = iter(file_data)
    frame = geopandas.GeoDataFrame()

    mocker.patch.object(frame, 'iterrows', return_value=iter_file_data)
    mocker.patch(
        'geopandas.read_file', return_value=frame
    )
    json_file = JSONFile(dir_name, file_name)
    mocker.patch.object(
        json_file, '_polygons_report', return_value='test'
    )
    json_file._reporters = {
        Polygon: json_file._polygons_report
    }
    with pytest.raises(ShapeNotSupported):
        json_file.generate_report()
