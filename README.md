# Homework AGREMO    
### Installation
1) Create new virtual env    
2) Use the package manager pip to install all dependencies.    
    ``` pip install -r requirements.txt ```
### Usage
To run server, make sure that you are in virtual env, just run:  ```   
python main.py ```
### Routes    
There are two routes:    
1) Convert file format from one to another. Supported: shape, geojson:
    ```/v0/rpc/convert_file/format/<string:convert_format>```   
2) Get file report. Supported: Polygon, LineString, Point:
    ```/v0/rpc/generate_report/format/<string:report_format>```  
  
  Both endpoints takes file in form-data format