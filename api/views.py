import json
import os
import tempfile

import inject
from dicttoxml import dicttoxml
from flask import request, send_file, Response
from flask.views import MethodView
from werkzeug.utils import secure_filename

from common.config import Config
from common.errors import (
    NoFilePart, InvalidFileName, ReportFormatNotSupported
)
from service.file import FileService


class FileUpload:
    config = inject.attr(Config)
    file_service = inject.attr(FileService)
    formatting_functions = {
        'json': json.dumps,
        'xml': dicttoxml
    }
    mime_types = {
        'json': 'application/json',
        'xml': 'application/xml'
    }

    def response(self, data,  format_type):
        try:
            response_data = self.formatting_functions[format_type](data)
        except KeyError:
            raise ReportFormatNotSupported()
        return Response(
            response=response_data,
            status=200,
            mimetype=self.mime_types[format_type]
        )

    def _upload_file(self):
        # check if the post request has the file part
        if 'file' not in request.files:
            raise NoFilePart()
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            raise InvalidFileName()

        filename = secure_filename(file.filename)
        dir = tempfile.mkdtemp()
        path = os.path.join(dir, filename)
        file.save(path)
        return dir, filename


class FileConvertView(FileUpload, MethodView):

    def post(self, convert_format):
        dir_path, filename = self._upload_file()
        path = self.file_service.convert(dir_path, filename, convert_format)
        return send_file(path, as_attachment=True)


class FileReportView(FileUpload, MethodView):

    def post(self, report_format):
        dir_path, filename = self._upload_file()
        report_data = self.file_service.report(dir_path, filename)
        return self.response(report_data, report_format)
