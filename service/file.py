from common.errors import FileTypeNotSupported
from common.utils import SHPFile, JSONFile


class FileService:

    def __init__(self):
        self._file_types = {
            'zip': SHPFile,
            'json': JSONFile,
        }

    def _get_file(self, tempdir, file):
        file_type = self._file_types.get(file.rsplit('.', 1)[1].lower())
        if not file_type:
            raise FileTypeNotSupported()
        return file_type(tempdir, file)

    def convert(self, tempdir, file, to):
        file_impl = self._get_file(tempdir, file)
        return file_impl.convert_to(to)

    def report(self, tempdir, file):
        file_impl = self._get_file(tempdir, file)
        return file_impl.generate_report()
