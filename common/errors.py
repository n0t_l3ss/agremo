from flask import jsonify


class APIError(Exception):
    status_code = 500

    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

    def to_dict(self):
        return {'message': self.message}


class BadRequest(APIError):
    status_code = 400


class InvalidFileFormatError(BadRequest):

    def __init__(self):
        APIError.__init__(self, 'Provided file format is not supported')


class NoFilePart(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'File part is not provided')


class InvalidFileName(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'File name is not valid')


class InvalidSHPFile(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'shp file is not valid')


class FileTypeNotSupported(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'file type is not supported')


class CannotConvertToSameType(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'cannot convert to same type')


class DifferentTypesNotSupported(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'different types not supported')


class ReportFormatNotSupported(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'report format not supported')


class ShapeNotSupported(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'provided shape type is not supported')


class ConversionFormatNotSupported(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'conversion format is not supported')


class FileNotGoodFormat(BadRequest):
    def __init__(self):
        APIError.__init__(self, 'file is not in good format')


class NotFoundError(APIError):
    status_code = 404

    def __init__(self):
        APIError.__init__(self, "requested resource doesn't exists")


class MethodNotAllowed(APIError):
    status_code = 405

    def __init__(self):
        APIError.__init__(self, "requested method is not allowed")


def api_error_handler(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
