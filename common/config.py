
class Config(object):

    CONFIG_ERROR = '{} is not {} type'

    def __init__(self, data):
        self.port = data['service']['port']
        if self.port is None or not isinstance(self.port, int):
            raise Exception(
                Config.CONFIG_ERROR.format('port', 'int')
            )
        self.debug = data['service']['debug']
        if self.debug is None or not isinstance(self.debug, bool):
            raise Exception(
                Config.CONFIG_ERROR.format('debug', 'bool')
            )
