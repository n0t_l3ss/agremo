import os
import zipfile
import abc

import fiona
import geopandas as gpd
from shapely.geometry import Polygon, LineString, Point

from common.errors import InvalidSHPFile, CannotConvertToSameType, \
    DifferentTypesNotSupported, ShapeNotSupported, \
    ConversionFormatNotSupported, FileNotGoodFormat


class ConvertibleFile:

    __metaclass__ = abc.ABCMeta

    METRIC_STANDARD = 32643

    def __init__(self, dir_path, file_path):
        self._dir_path = dir_path
        self._file_path = file_path
        self._converters = {
            'json': self._to_json,
            'shp':  self._to_shp
        }
        self._file = self._read_file()
        self._reporters = {
            Polygon: self._polygons_report,
            LineString: self._line_strings_report,
            Point: self._points_report
        }

    def generate_file_name(self):
        return self._file_path.rsplit('.', 1)[0].lower()

    def _to_json(self):
        file_name = self.generate_file_name()
        output_file = os.path.join(self._dir_path, file_name) + '.json'
        with open(
            output_file, 'w'
        ) as file:
            file.write(self._file.to_json())
        return output_file

    def _to_shp(self):
        file_name = self.generate_file_name()
        output_dir = os.path.join(self._dir_path, 'output')
        os.makedirs(output_dir)
        shp_file = os.path.join(output_dir, file_name) + '.shp'
        output_file = os.path.join(self._dir_path, file_name) + '.zip'
        with zipfile.ZipFile(
            output_file,
            'w',
            zipfile.ZIP_DEFLATED
        ) as file:
            self._file.to_file(
                shp_file
            )

            for root, dirs, files in os.walk(
                output_dir
            ):
                for x in files:
                    file.write(os.path.join(
                        output_dir, x), x
                    )
        return output_file

    def convert_to(self, ext_type):
        try:
            return self._converters[ext_type]()
        except KeyError:
            raise ConversionFormatNotSupported()

    @abc.abstractmethod
    def _read_file(self):
        raise NotImplementedError()

    def _generic_report(self, attr_to_sum,  cls):
        file = self._file.to_crs(epsg=self.METRIC_STANDARD)
        max_v = 0
        min_v = -1
        count = 0
        for _, x in file.iterrows():
            if not isinstance(x['geometry'], cls):
                raise DifferentTypesNotSupported()
            val = getattr(x['geometry'], attr_to_sum)
            if min_v == -1 or val < min_v:
                min_v = val
            if val > max_v:
                max_v = val
            count = count + 1

        return max_v, min_v, count

    def _polygons_report(self):
        max_v, min_v, count = self._generic_report('area', Polygon)
        return {
            'max_area': max_v,
            'min_area': min_v,
            'polygon_count': count
        }

    def _line_strings_report(self):
        max_v, min_v, count = self._generic_report('length', LineString)
        return {
            'max_length': max_v,
            'min_length': min_v,
            'line_string_count': count
        }

    def _points_report(self):
        count = 0
        file = self._file.to_crs(epsg=self.METRIC_STANDARD)
        for _, x in file.iterrows():
            if not isinstance(x['geometry'], Point):
                raise DifferentTypesNotSupported()
            count = count + 1
        minx, miny, maxx, maxy = file.total_bounds
        return {
            'points_count': count,
            'bbox': {
                'minx': minx,
                'miny': miny,
                'maxx': maxx,
                'maxy': maxy
            }
        }

    def generate_report(self):
        try:
            return self._reporters[
                next(self._file.iterrows())[1]['geometry'].__class__
            ]()
        except KeyError:
            raise ShapeNotSupported()


class SHPFile(ConvertibleFile):

    def __init__(self, dir_path, file_path):
        self.full_path = os.path.join(dir_path, file_path)
        super().__init__(dir_path, file_path)

    def _read_file(self):
        try:
            z = zipfile.ZipFile(self.full_path)
            z.extractall(path=self._dir_path)
            shp_files = [
                y for y in sorted(z.namelist()) if y.endswith('shp')
            ]
            if len(shp_files) != 1:
                raise InvalidSHPFile()
            file = gpd.read_file(os.path.join(self._dir_path, shp_files[0]))
            return file
        except (fiona.errors.DriverError, zipfile.BadZipFile):
            raise FileNotGoodFormat()

    def _to_shp(self):
        raise CannotConvertToSameType()


class JSONFile(ConvertibleFile):

    def __init__(self, dir_path, file_path):
        super().__init__(dir_path, file_path)

    def _read_file(self):
        try:
            return gpd.read_file(
                os.path.join(self._dir_path, self._file_path)
            )
        except fiona.errors.DriverError:
            raise FileNotGoodFormat()

    def _to_json(self):
        raise CannotConvertToSameType()
