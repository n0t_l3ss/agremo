import inject

from flask import Flask
from api.views import FileConvertView, FileReportView
from common.errors import APIError, MethodNotAllowed
from common.config import Config
import yaml

from common.errors import api_error_handler, NotFoundError
from service.file import FileService


def ioc(binder):

    with open("./config/config.yaml", 'r') as stream:
        try:
            config = Config(yaml.safe_load(stream))
            binder.bind(Config, config)
            file_service = FileService()
            file_view = FileConvertView()
            binder.bind(FileService, file_service)
            binder.bind(FileConvertView, file_view)
        except yaml.YAMLError as exc:
            print(exc)


if __name__ == '__main__':
    inject.configure_once(ioc)
    app = Flask(__name__)
    conf = inject.instance(Config)
    app.debug = conf.debug
    app.add_url_rule(
        '/v0/rpc/convert_file/format/<string:convert_format>', 'convert',
        view_func=FileConvertView.as_view('file_convert_view')
    )
    app.add_url_rule(
        '/v0/rpc/generate_report/format/<string:report_format>', 'report',
        view_func=FileReportView.as_view('file_report_view')
    )
    app.register_error_handler(APIError, api_error_handler)
    app.register_error_handler(404, lambda _:  api_error_handler(
        NotFoundError()
    ))
    app.register_error_handler(405, lambda _:  api_error_handler(
        MethodNotAllowed()
    ))
    app.run(port=conf.port)
